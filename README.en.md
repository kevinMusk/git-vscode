# git-vscode下载和安装

#### Description
从git、vscode安装、配置到关联gitee远程仓库的详细教程

#### Software Architecture
Software architecture description

#### Installation

gitee安装和使用

1.gitee的注册  
​        注册步骤：
​	a. 访问 https://gitee.co网站后点击注册
​	b. 填写姓名、个人空间地址 、手机或邮箱、密码即可

2.创建远程仓库
​        创建步骤：

```
a.登录个人主页后，点击右上角 + 进行创建
b.填写仓库名称、路径、项目介绍、是否开源、语言、开源协议（推荐GPL-3.0）
```

3.克隆仓库到本地
​	参考地址：https://www.bilibili.com/video/BV1hf4y1W7yT?from=search&seid=10127404889610163328&spm_id_from=333.337.0.0
​	a.安装git 下载地址：https://git-scm.com/download/win
​	b.安装tortoisegit     下载地址：https://tortoisegit.org/download/   安装地址：https://blog.csdn.net/weixin_39653761/article/details/111347896
​	c.提交代码三板斧
​		i.add
​		ii.commit
​		iii.push

4.VS Code管理gitee项目
​	参考地址：https://www.bilibili.com/video/BV18Z4y1P73M?from=search&seid=10127404889610163328&spm_id_from=333.337.0.0
​	a.安装vs code 下载地址：https://code.visualstudio.com/Download

5.VS Code管理github项目
​	参考地址：https://www.bilibili.com/video/BV1dK411p7RF/?spm_id_from=autoNext	

GPL协议的理解（开源与商用、免费与收费）参考链接：https://blog.csdn.net/testcs_dn/article/details/38496107

如果你用了我的 GPL协议开发的软件，那么你的软件也必须要开源；
如果你不开源，那么就不能使用我的软件，你是否把你的软件商用和我没关系；
如果你不能遵循GPL协议来开源，那么你付再多的钱也不能用GPL的软件（双向授权除外 -》详解：https://www.cnblogs.com/goldenstones/p/8780091.html）

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
